package uk.sliske.rsbot.gameselector;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import uk.sliske.util.IO.FileUtils;
import uk.sliske.util.IO.WebIO;

public class GameSelector extends JFrame {
	private static final long		serialVersionUID	= 1L;

	private static final String		SETTING_FILE_NAME	= "rsbot loader";

	private JLabel					contentPane;

	private HashMap<String, String>	settings;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					GameSelector frame = new GameSelector();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GameSelector() {
		settings = FileUtils.loadSettings(SETTING_FILE_NAME);

		setTitle("RSBot Game Selector");
		setIconImage(loadImage("02db75eeb0"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1024, 650);
		contentPane = new JLabel(new ImageIcon(loadImage("f25f631f7a", getWidth(), getHeight())));
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		final int offset = 50;

		JLabel rs3 = new JLabel("");
		rs3.setBounds(offset, offset, 400, 150);
		rs3.addMouseListener(gameListener(GAME.RS3));
		rs3.setBackground(Color.BLACK);
		rs3.setIcon(new ImageIcon(loadImage("629b4993c4"), "Runescape 3"));
		contentPane.add(rs3);

		JLabel ds = new JLabel("");
		ds.setBounds(getWidth() - 400 - offset, offset, 400, 150);
		ds.addMouseListener(gameListener(GAME.DARKSCAPE));
		ds.setBackground(Color.BLACK);
		ds.setIcon(new ImageIcon(loadImage("ecc7b4916f"), "DarkScape"));
		contentPane.add(ds);

		JLabel os = new JLabel("");
		os.setBounds(getWidth() / 2 - 200, offset * 2 + 150, 400, 150);
		os.addMouseListener(gameListener(GAME.OLDSCHOOL));
		os.setBackground(Color.BLACK);
		os.setIcon(new ImageIcon(loadImage("bfab43cba3"), "Oldschool"));
		contentPane.add(os);

		JLabel rsbot_ = new SettingPanel(settings, SETTING.RSBOT);
		rsbot_.setBounds(offset / 2, offset * 3 + 300, 450, 60);
		contentPane.add(rsbot_);

		JLabel launcher_ = new SettingPanel(settings, SETTING.LAUNCHER);
		launcher_.setBounds(offset / 2, (int) (offset * 3.2 + 360), 450, 60);
		contentPane.add(launcher_);

		JLabel java_ = new SettingPanel(settings, SETTING.JAVA);
		java_.setBounds(getWidth() - 450 - offset, offset * 3 + 300, 450, 60);
		contentPane.add(java_);

		JLabel scripts = new SettingPanel(settings, SETTING.SCRIPTS);
		scripts.setBounds(getWidth() - 450 - offset, (int) (offset * 3.2 + 360), 450, 60);
		contentPane.add(scripts);

		setLocationRelativeTo(null);
		setResizable(false);
	}

	public static Image loadImage(final String name, int width, int height) {
		return loadImage(name).getScaledInstance(width, height, 0);
	}

	public static BufferedImage loadImage(final String name) {
		try {
			File f = new File(FileUtils.formatPath(".", "res", name + ".png"));
			if (!f.exists())
				f = new File("./res/" + name + ".png");
			BufferedImage image = null;
			if (f.exists())
				image = ImageIO.read(f);
			if (image == null)
				image = WebIO.loadBImageFromWeb("http://sliske.uk/img/captures/" + name + ".png");

			return image;
		} catch (Exception e1) {
		}

		return null;
	}

	public String checkSetting(final HashMap<String, String> settings, final String key, final String def) {
		if (settings.containsKey(key))
			return settings.get(key);
		settings.put(key, def);
		return def;
	}

	public void close() {
		setVisible(false);
		FileUtils.saveSettings(SETTING_FILE_NAME, settings);
		dispose();
	}

	private MouseAdapter gameListener(final GAME game) {
		return new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (start(game))
					close();
			}
		};
	}

	public boolean start(GAME game) {
		try {
			String run = SETTING.JAVA.extractf(settings, "\"", "\"", true);
			run += " -jar ";
			run += SETTING.SCRIPTS.extractf(settings, "-Djava.search.path=\"", "\" ", false);
			run += "-Dsun.java2d.nodraw=true ";
			run += "-Dcom.jagex.config=" + game.config + " ";
			run += "-Xmx512m ";
			run += "-Xss2m ";
			run += "-XX:CompileThreshold=1500 ";
			run += "-Xincgc ";
			run += "-XX:+UseConcMarkSweepGC ";
			run += "-XX:+UseParNewGC ";
			run += SETTING.RSBOT.extractf(settings, "-javaagent:", " ", true);
			run += SETTING.LAUNCHER.extractf(settings, "\"", "\" ", true);
			run += "runescape ";
			Runtime.getRuntime().exec(run);
			System.out.println(run);
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	enum GAME {
		RS3("http://www.runescape.com/l=en/jav_config.ws"),
		DARKSCAPE("http://www.runescape.com/jav_config_beta.ws"),
		OLDSCHOOL("http://oldschool.runescape.com/l=en/jav_config.ws");

		final String	config;

		GAME(String config) {
			this.config = config;
		}
	}

	public enum SETTING {
		RSBOT(
				"rsbot",
				FileUtils.USER_HOME + "\\Desktop\\RSBot.jar",
				"Select RSBot application to use"),
		SCRIPTS("scripts", "", "Select your scripts bin directory"),
		JAVA("java", GameSelector.getJavaPath(), "Select java version to use"),
		LAUNCHER(
					"launcher",
					FileUtils.USER_HOME + "\\jagexcache\\jagexlauncher\\Bin\\jagexappletviewer.jar",
					"Select Jagex launcher file to use");

		public final String	key;
		public final String	defaultValue;
		public final String	fileSelectorTitle;

		SETTING(String key, String def, String title) {
			this.key = key;
			this.defaultValue = def;
			this.fileSelectorTitle = title;
		}

		public String extract(HashMap<String, String> settings) {
			if (settings.containsKey(key))
				return settings.get(key);
			settings.put(key, defaultValue);
			return defaultValue;
		}

		public String extractf(HashMap<String, String> settings, String prefix, String suffix, boolean b) {
			final String s = extract(settings);
			if (s.length() > 0 || b)
				return prefix + s + suffix;
			return "";
		}

	}

	public static String getJavaPath() {
		try {
			return getJreExecutable().getAbsolutePath();
		} catch (FileNotFoundException e) {
		}
		return "java";
	}

	public static boolean isWindows() {
		String os = System.getProperty("os.name");
		if (os == null) {
			throw new IllegalStateException("os.name");
		}
		os = os.toLowerCase();
		return os.startsWith("windows");
	}

	public static File getJreExecutable() throws FileNotFoundException {
		String jreDirectory = System.getProperty("java.home");
		if (jreDirectory == null) {
			throw new IllegalStateException("java.home");
		}
		File exe;
		if (isWindows()) {
			exe = new File(jreDirectory, "bin/java.exe");
		} else {
			exe = new File(jreDirectory, "bin/java");
		}
		if (!exe.isFile()) {
			throw new FileNotFoundException(exe.toString());
		}
		return exe;
	}

}
