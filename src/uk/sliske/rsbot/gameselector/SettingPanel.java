package uk.sliske.rsbot.gameselector;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.concurrent.Callable;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import uk.sliske.rsbot.gameselector.GameSelector.SETTING;

public class SettingPanel extends JLabel {
	private static final long	serialVersionUID	= 1L;
	
	public SettingPanel(final HashMap<String, String> settings, final SETTING setting) {
		super(new ImageIcon(GameSelector.loadImage("55ad065f31")));		
		setBackground(Color.BLACK);		
		setLayout(null);

		JLabel lbl = new JLabel(setting.name().toLowerCase());
		lbl.setBounds(0, 2, 72, 57);
		lbl.setHorizontalAlignment(SwingConstants.CENTER);
		
		add(lbl);

		JTextField textField = new JTextField(setting.extract(settings));
		textField.setEditable(false);
		textField.setBounds(74, 2, 342, 57);
		add(textField);
		

		JLabel button = new JLabel("#");
		button.setHorizontalAlignment(SwingConstants.CENTER);
		button.setBackground(Color.BLACK);
		button.setBounds(416, 2, 33, 57);
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new FileSelector(settings, setting, new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						textField.setText(setting.extract(settings));
						return null;
					}
				}).setVisible(true);
			}
		});		
		add(button);

	}
}
