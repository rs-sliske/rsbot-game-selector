package uk.sliske.rsbot.gameselector;

import java.awt.BorderLayout;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.Callable;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import uk.sliske.rsbot.gameselector.GameSelector.SETTING;

public class FileSelector extends JFrame {
	private static final long	serialVersionUID	= 1L;
	private JPanel				contentPane;

	public FileSelector(final HashMap<String, String> map, final SETTING setting, Callable<?> callback) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JFileChooser fileChooser = new JFileChooser() {
			private static final long	serialVersionUID	= 1L;

			@Override
			public void approveSelection() {
				super.approveSelection();
				File f = getSelectedFile();
				if (f != null && f.exists())
					map.put(setting.key, f.getAbsolutePath());
				if (callback != null)
					try {
						callback.call();
					} catch (Exception e) {
					}
				dispose();
			}

			@Override
			public void cancelSelection() {
				super.cancelSelection();
				dispose();
			}
		};
		boolean b = true;
		if (setting == GameSelector.SETTING.JAVA) {
			if (GameSelector.isWindows()) {
				fileChooser
						.addChoosableFileFilter(new FileNameExtensionFilter("Executable", "exe"));
				b = false;
			}
		} else
			if (setting == GameSelector.SETTING.SCRIPTS) {
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			} else {

				fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Java Archive File",
						"jar"));
				b = false;
			}
		fileChooser.setAcceptAllFileFilterUsed(b);
		contentPane.add(fileChooser, BorderLayout.CENTER);

		pack();
	}

}
