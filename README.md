# README #

### What is this repository for? ###

* RSBot Game Selector  
	http://www.powerbot.org/community/topic/1296157-/
	
* Version 1.3


### How do I get set up? ###

* Pre-compiled  
	https://bitbucket.org/rs-sliske/rsbot-game-selector/downloads/Rsbot%20Loader%20v1.3.jar
	
* Configuration  
	rsbot : this needs to be the latest universal (.jar) version of RSBot ( http://www.powerbot.org/download/?jar )   
	  
	launcher : this is the jagexappletviewer.jar file which can be found in $home -> jagexcache 
	     
	java : the java version you wish to use, can normally be left as default    
	  
	scripts : if you wish to test a local script, set this to your projects bin folder   
	   
	
* Dependencies    
	if building from the source you will also need https://bitbucket.org/rs-sliske/utils


### Who do I talk to? ###

* skype : custard.pb
* email : admin@sliske.uk
* livechat : sliske.uk
* powerbot : http://www.powerbot.org/community/user/9961-sliske/